"""
Example translators for conversion between related variables.
"""

def gammaTranslator( fitDict ):
    #Calculates gamma in terms of gsds and gd
    try:
        gd = fitDict[ 'gammaD' ]
        gsgd = fitDict[ 'gsgd' ]
        gamma = gsgd+gd
        return gamma
    except:
        print(' \n Translator for gamma called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def gsgdTranslator( fitDict ):
    #Calculates gamma in terms of gsds and gd
    try:
        gd = fitDict[ 'gammaD' ]
        gamma = fitDict[ 'gamma' ]
        gsgd = gamma-gd
        return gsgd
    except:
        print(' \n Translator for gsgd called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def ghgdTranslator( fitDict ):
    #Calculates ghgd in terms of the fit parameters  (in this case gsgd and deltaGammas)
    try:
        gd = fitDict[ 'gammaD' ]
        gamma = fitDict[ 'gamma' ]
        dg = fitDict[ 'deltaGammas' ]
        ghgd = gamma-gd-dg/2
        return ghgd
    except:
        print(' \n Translator for ghgd called, but it cant find parameters it can use - exiting \n')
        sys.exit()
        
def AddAllTranslators( reslist ):
    reslist.addParameterTranslator( 'gamma', gammaTranslator )
    reslist.addParameterTranslator( 'gsgd', gsgdTranslator )
    reslist.addParameterTranslator( 'ghgd', ghgdTranslator )
