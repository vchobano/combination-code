###############################
### 2020 Veronika Chobanova ### 
###############################

import sys
import json
sys.path.append("../../code/python/")
from HelperFunctions import showIminuitResult, showIminuitMinosResult, showIminuitResultCorrmat, outputMinuitResult, EvaluateForMinuit
from HelperFunctions import printLatexTable, printChi2nDoF, makeResidualPlot
from ResultSet import *
from iminuit import Minuit
import pylab as pl
sys.path.append("./")
from Translators import *
from scipy import stats, special

#Starting values for fit parameters
d_start = {'phis': 'None', 'Gs': 1., 'DGs': 'None', 'taus': 1., 'tausL': 1., 'tausH': 1., 'DGs/Gs': 1.}

outputpath = "outputs/"
        
def doAverage( reslist, pars, outputJSONname, fix_dms = 0, fix_lamb=1):
    np.set_printoptions(1)

    print('\n =================================================================== ')

    #Add all translators.  They must appear in the Translators.py module
    AddAllTranslators( reslist )
   
    #Create the FCN.
    #Parameter order is driven by the order of the list of 'pars' above
    def fcn( vals ):
        fitDict = dict(list(zip(pars,vals)))
        return reslist.getChisq( fitDict )
   
    # Get the dictionary with all parameters, their start values and their initial setepsize (errors)
    svals = reslist.getStartValues( pars )

    for s in d_start.keys():
        if d_start[s]!="None" and s in svals.keys():
            svals.update({s: float(d_start[s])}) 
    #Fix any parameters you want to - examples below
    if (fix_dms):
        svals.update({'fix_dms':True})
        svals.update({'dms':fix_dms})
        svals.update({'error_dms':0.021})
    
    if (fix_lamb and "lamb" in svals.keys()):
        svals.update({'fix_lamb':True})
        svals.update({'lamb':fix_lamb})
        svals.update({'error_lamb':1.})

    # Now get the numerical list of starting values out of the dictionary in the correct order for the Minuit constructor
    # The names and order of parameters is given by pars (this is mandatory argument for this constructor even though all the information is in svals )
    start =[ svals[p] for p in pars ]
    
    eval = EvaluateForMinuit( reslist, pars )
    
    # Create Minimiser with special constructor.
    m = Minuit(eval.fcn, start, name=pars)
    m.tol= m.tol*0.0001
    
    #Do fit
    print('\n Starting Minuit fit')
    m.migrad()
    
    #Extract results if you need them.
    #valDict =  m.values
    #errDict = m.errors
    #corrmat = m.matrix(True)
 
    #Print the final averages
    showIminuitResult( m )

    m.minos( )
    showIminuitMinosResult( m )
    
    #Show the simple average for comparison
    reslist.showSimpleAverage(paramorder = pars)
 
    #Show the final correlation matrix
    showIminuitResultCorrmat( m )

    #output the full result in JSON format for use in a subsequent average
    #reslist.outputSimpleAverage(outputFileName='LHCbSimpleAverageResultSet.json', title='Simp.Avg.' )

    outputMinuitResult( m, outputFileName=outputpath+outputJSONname, title=outputJSONname.replace(".json", ""), minos=True )
    
    res = readResultListJSON([outputJSONname], inputpath=outputpath)
    
    latexLabels = {'phis': '$\phi_{s}$',
                'Gs': ' $\Gamma_{s}$ ',
                'Gd': ' $\Gamma_{d}$ ',
                'DGs': ' $\Delta\Gamma_{s}$ ', 
                'DGs/Gs': '\Delta\Gamma_{s}/\Gamma_{s}',
                'taus': ' $\tau_{s}$ ', 
                'tausL': ' $\tau_{sL}$ ', 
                'tausH': ' $\tau_{sH}$ ', 
                'dms': '$\Delta m_{s}$',
                'lamb': '$|\lambda|$',
                'para': '$\delta_{\parallel}$',
                'perp': '$\delta_{\perp}$',
                'AzeroSq': '$|A_{0}|^2$',
                'AperpSq': '$|A_{\perp}|^2$',
                'AsSqATLAS': '$|A^{\rm ATLAS}_{\rm S}|^2$',
                'perpSATLAS': '$(\delta_{\perp}-\delta_{S})^{\rm ATLAS}$',
                'AsSqCMS': '$|A^{\rm CMS}_{\rm S}|^2$',
                'SperpCMS': '$(\delta_{S}-\delta_{\perp})^{\rm CMS}$',
                'AsSqLHCb': '$|A^{\rm LHCb}_{\rm S}|^2$',
                'SperpLHCb': '$(\delta_{S}-\delta_{\perp})^{\rm LHCb}$'}
    
    printLatexTable(m, latexLabels, outputFileName=outputpath+'CombinationCorrmatTable'+outputJSONname.replace(".json", ".tex"))
    
    return m, res

#-------------- Main -------------
def main():
    #Set up input file list
    print('\nTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT')
    print('\ResultSet Combination Code ')
            
    #inputs - all but LHCb
    inputfileATLAS = ["../inputs/ATLAS_JpsiKK_a_Spring2021.json"]
    inputfileCMS = ["../inputs/CMS_JpsiKK_Spring2021.json"]
    initialinputs = ["../inputs/LHCb_JpsiKK_Spring2021.json", "../inputs/LHCb_JpsieeKK_Spring2021.json"]
        
    #Precombine LHCb for illustration
    reslist = readResultListJSON(initialinputs, inputpath="")
    reslist.setErrorTreatmentFlags( doStatCorr=True, doSyst=True, doSystCorr=True, doIntercorr=False )
    reslist.sanityCheck( )
                
    inputfileLHCb = "LHCb_JpsiKK_WriteUp2021.json" 
    parameters    = ["AzeroSq", "AperpSq", "AsSqCMS", "para", "perp", "SperpCMS", "Gs", "DGs", "dms", "lamb", "phis", "Gd"]
    m, res = doAverage( reslist, parameters, inputfileLHCb)
    inputfilesLHCb = ["outputs/"+inputfileLHCb]
                
    # Make the combinations
    ## Using precombined LHCb output
    initialinputs = inputfilesLHCb+inputfileATLAS+inputfileCMS
    
    reslist = readResultListJSON(initialinputs, inputpath="")
    reslist.setErrorTreatmentFlags( doStatCorr=True, doSyst=True, doSystCorr=True, doIntercorr=True )
    reslist.sanityCheck( )

    m, resphisGs = doAverage( reslist, parameters, "WriteUp2021JpsiKKa.json", fix_dms=17.757)
      
    parametersRat = ["AzeroSq", "AperpSq", "AsSqCMS", "para", "perp", "SperpCMS", "taus", "DGs/Gs", "dms", "lamb", "phis", "Gd"]
    m, resphisGsRat = doAverage( reslist, parametersRat, "WriteUp2021JpsiKKRata.json", fix_dms=17.757)
        
    parametersLH  = ["AzeroSq", "AperpSq", "AsSqCMS", "para", "perp", "SperpCMS", "tausL", "tausH", "dms", "lamb", "phis", "Gd"]
    m, resphisGsLH = doAverage( reslist, parametersLH, "WriteUp2021JpsiKKLHa.json", fix_dms=17.757)
        
    #GsDGsPlot(initialinputs, outputpath+JpsihhOutputFileName, ["LHCb 4.9 fb$^{-1}$", "ATLAS 99.7 fb$^{-1}$", "CMS 116.1 fb$^{-1}$", "CDF 9.6 fb$^{-1}$", "D0 8 fb$^{-1}$", "Combined$^{*}$"], round(factors["DGs"],2), round(factors["Gs"],2), thisVersion, thisYear, mode=Mode)
    
    exit()

main()
