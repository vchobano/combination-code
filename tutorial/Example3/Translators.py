import sys
c_light = 299.792458

def GsTranslator( fitDict ):
    #Calculates Gs in terms of gsds and gd
    if ('Gd' in fitDict) and ('gsgd' in fitDict ):
        gd = fitDict[ 'Gd' ]
        gsgd = fitDict[ 'gsgd' ]
        Gs = gsgd+gd
        return Gs
    elif ('Gd' in fitDict) and ('gsgdRatio' in fitDict ):
        gd = fitDict[ 'Gd' ]
        gsgdr = fitDict[ 'gsgdRatio' ]
        Gs = gsgdr*gd
        return Gs
    elif ('taus' in fitDict ):
        Gs = 1./fitDict[ 'taus' ]
        return Gs
    elif ('tausL' in fitDict ) and ('DGs' in fitDict ):
        Gs = 1./fitDict[ 'tausL' ] - fitDict[ 'DGs' ]/2.
        return Gs
    elif ('tausH' in fitDict ) and ('DGs' in fitDict ):
        Gs = 1./fitDict[ 'tausH' ] + fitDict[ 'DGs' ]/2.
        return Gs
    elif ('tausL' in fitDict ) and ('tausH' in fitDict ):
        Gs = (1./fitDict[ 'tausL' ] + 1./fitDict[ 'tausH' ])/2.
        return Gs
    elif ('DGs/Gs' in fitDict ) and ('DGs' in fitDict ):
        Gs = fitDict[ 'DGs' ]/fitDict[ 'DGs/Gs' ]
        return Gs
    else:
        print(' \n Translator for Gs called, but it cant find parameters it can use - exiting \n')
        sys.exit()
    
def DGsTranslator( fitDict ):
    if ('tausL' in fitDict ) and ('tausH' in fitDict ):
        DGs = 1./fitDict[ 'tausL' ] - 1./fitDict[ 'tausH' ]
        return DGs
    elif ('DGs/Gs' in fitDict ) and ('Gs' in fitDict ):
        DGs = fitDict[ 'DGs/Gs' ]*fitDict[ 'Gs' ]
        return DGs
    elif ('DGs/Gs' in fitDict ) and ('taus' in fitDict ):
        DGs = fitDict[ 'DGs/Gs' ]/fitDict[ 'taus' ]
        return DGs
    else:
        print(' \n Translator for DGs called, but it cant find parameters it can use - exiting \n')
        sys.exit()
    
def tausTranslator( fitDict ):
    if (('DGs/Gs' in fitDict) and ('DGs' in fitDict )):
        taus = c_light/(fitDict[ 'DGs' ]/fitDict[ 'DGs/Gs' ])
        return taus
    elif ('Gs' in fitDict):
        taus = c_light/fitDict[ 'Gs' ]
    elif ('taus' in fitDict):
        taus = fitDict[ 'taus' ]
        return taus
    elif (('ghgd' in fitDict) and ('DGs' in fitDict ) and ('Gd' in fitDict )):
        taus = c_light/(fitDict[ 'ghgd' ] + fitDict[ 'Gd' ] + fitDict[ 'DGs' ]/2.)
        return taus
    else:
        print(' \n Translator for taus called, but it cant find parameters it can use - exiting \n')
        sys.exit()
    
def tausLTranslator( fitDict ):
    if('Gs' in fitDict ) and ('DGs' in fitDict ):
        tausL = 1./(fitDict[ 'Gs' ] + fitDict[ 'DGs' ]/2.)
        return tausL
    else:
        print(' \n Translator for tausL called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def tausHTranslator( fitDict ):
    if('Gs' in fitDict ) and ('DGs' in fitDict ):
        tausH = 1./(fitDict[ 'Gs' ] - fitDict[ 'DGs' ]/2.)
        return tausH
    else:
        print(' \n Translator for tausL called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def tausLHTranslator( fitDict ):
    if('Gs' in fitDict ) and ('DGs' in fitDict ):
        tausL = tausLTranslator( fitDict )
        tausH = tausHTranslator( fitDict )
        return (tausL, tausH)
    else:
        print(' \n Translator for tausL, tausH called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def gsgdTranslator( fitDict ):
    #Calculates Gs in terms of gsds and gd
    if ('Gd' in fitDict) and ('Gs' in fitDict ):
        gd = fitDict[ 'Gd' ]
        Gs = fitDict[ 'Gs' ]
        gsgd = Gs-gd
        return gsgd
    elif ('Gd' in fitDict) and ('gsgdRatio' in fitDict ):
        gd = fitDict[ 'Gd' ]
        gsgdr = fitDict[ 'gsgdRatio' ]
        Gs = gsgdr*gd
        gsgd = Gs-gd
        return gsgd
    else:
        print(' \n Translator for gsgd called, but it cant find parameters it can use - exiting \n')
        sys.exit()


def ghgdTranslator( fitDict ):
        #Calculates ghgd in terms of the fit parameters  (in this case gsgd and DGs)
    if ('Gd' in fitDict) and ('Gs' in fitDict ) and ('DGs' in fitDict ):
        gd = fitDict[ 'Gd' ]
        Gs = fitDict[ 'Gs' ]
        dg = fitDict[ 'DGs' ]
        ghgd = Gs-gd-dg/2
        return ghgd
    elif ('Gd' in fitDict) and ('taus' in fitDict ) and ('DGs' in fitDict ):
        gd = fitDict[ 'Gd' ]
        Gs = 1./fitDict[ 'taus' ]
        dg = fitDict[ 'DGs' ]
        ghgd = Gs-gd-dg/2
        return ghgd
    elif ('Gd' in fitDict) and ('gsgdRatio' in fitDict ) and ('DGs' in fitDict ):
        gd = fitDict[ 'Gd' ]
        gsgdr = fitDict[ 'rat' ]
        dg = fitDict[ 'DGs' ]
        Gs = gsgdr*gd
        ghgd = Gs-gd-dg/2
        return ghgd
    elif ('Gd' in fitDict) and ('tausH' in fitDict ):
        gd = fitDict[ 'Gd' ]
        GH = 1./fitDict[ 'tausH' ]
        ghgd = GH-gd
        return ghgd
    else:
        print(' \n Translator for ghgd called, but it cant find parameters it can use - exiting \n')
        sys.exit()


def AparaSqTranslator( fitDict ):
    #Calculates AparaSq in terms of the fit parameters
    try:
        AperpSq = fitDict[ 'AperpSq' ]
        AzeroSq = fitDict[ 'AzeroSq' ]
        AparaSq = 1-AperpSq-AzeroSq
        return AparaSq
    except:
        print(' \n Translator for AparaSq called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def perpSTranslator( fitDict ):
    #Calculates perpS in terms of the fit parameters  (in this case Sperp)
    try:
        Sperp = fitDict[ 'Sperp' ]
        perpS = -Sperp
        return perpS
    except:
        print(' \n Translator for perpS called, but it cant find parameters it can use - exiting \n')
        sys.exit()
    
def ctauTranslator( fitDict ):
    #Calculates ctau in terms of the fit parameters  (in this case Gs)
    try:
        Gs = fitDict[ 'Gs' ]
        ctau = 299.792458/Gs
        return ctau
    except:
        print(' \n Translator for ctau called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def SperpCMSTranslator ( fitDict ):
    #Calculates an phas dS-dpe for CMS in the ATLAS region
    try:
        perpS = fitDict[ 'perpSATLAS' ]
        SperpCMS = -perpS #- (-0.02)# [1008.5, 1030.5]
        return SperpCMS
    except:
        print(' \n Translator for delta_S - delta_perp CMS called, but it cant find parameters it can use - exiting \n')
        sys.exit()
        
def perpSATLASTranslator ( fitDict ):
    #Calculates an phas dS-dpe for CMS in the ATLAS region
    try:
        Sperp = fitDict[ 'SperpCMS' ]
        perpSATLAS = -Sperp #- (-0.02)# [1009.5, 1029.5]
        return perpSATLAS
    except:
        print(' \n Translator for delta_S - delta_perp ATLAS called, but it cant find parameters it can use - exiting \n')
        sys.exit()
        
def SperpLHCbTranslator ( fitDict ):
    #Calculates an phas dS-dpe for LHCb in the CMS region
    try:
        Sperp = fitDict[ 'SperpCMS' ]
        SperpLHCb = Sperp #+ (-0.02)# [1009.5, 1029.5]
        return SperpLHCb
    except:
        print(' \n Translator for delta_S - delta_perp LHCb called, but it cant find parameters it can use - exiting \n')
        sys.exit()
        
def AsSqCMSTranslator ( fitDict ):
    #Calculates a total faction fS for CMS in the ATLAS region
    try:
        AsSq = fitDict[ 'AsSqATLAS' ]
        AsSqCMS = AsSq*(1.775/1.952)/(AsSq*(1.775/1.952)+(1.-AsSq)*(866.29/878.14))# [1008.5, 1030.5]
        return AsSqCMS
    except:
        print(' \n Translator for AsSq CMS called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def AsSqATLASTranslator ( fitDict ):
    #Calculates a total faction fS for CMS in the ATLAS region
    try:
        AsSq = fitDict[ 'AsSqCMS' ]
        AsSqATLAS = AsSq*(1.952/1.775)/(AsSq*(1.952/1.775)+(1.-AsSq)*(878.14/866.29))# [1009.5, 1029.5]
        return AsSqATLAS
    except:
        print(' \n Translator for AsSq ATLAS called, but it cant find parameters it can use - exiting \n')
        sys.exit()
        
def AsSqLHCbTranslator ( fitDict ):
    #Calculates a total faction fS for LHCb in the CMS region
    try:
        AsSq = fitDict[ 'AsSqCMS' ]
        AsSqLHCb = AsSq*(5.235/1.775)/(AsSq*(5.235/1.775)+(1.-AsSq)*(958.854/866.29))# [1009.5, 1029.5]
        return AsSqLHCb
    except:
        print(' \n Translator for AsSq ATLAS called, but it cant find parameters it can use - exiting \n')
        sys.exit()

def AddAllTranslators( reslist ):
    reslist.addParameterTranslator( 'taus', tausTranslator)
    reslist.addParameterTranslator( 'tausL', tausLTranslator)
    reslist.addParameterTranslator( 'tausH', tausHTranslator)
    reslist.addParameterTranslator( 'Gs', GsTranslator )
    reslist.addParameterTranslator( 'DGs', DGsTranslator )
    reslist.addParameterTranslator( 'gsgd', gsgdTranslator )
    reslist.addParameterTranslator( 'ghgd', ghgdTranslator )
    reslist.addParameterTranslator( 'AparaSq', AparaSqTranslator )
    reslist.addParameterTranslator( 'perpSATLAS', perpSATLASTranslator )
    reslist.addParameterTranslator( 'SperpCMS', SperpCMSTranslator )
    reslist.addParameterTranslator( 'SperpLHCb', SperpLHCbTranslator )
    reslist.addParameterTranslator( 'AsSqATLAS', AsSqATLASTranslator )
    reslist.addParameterTranslator( 'AsSqCMS', AsSqCMSTranslator )
    reslist.addParameterTranslator( 'AsSqLHCb', AsSqLHCbTranslator )
    reslist.addParameterTranslator( 'ctau', ctauTranslator )
