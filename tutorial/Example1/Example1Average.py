###############################
### 2020 Veronika Chobanova ### 
###############################

import sys
import json
sys.path.append("../../code/python/")
from HelperFunctions import showIminuitResult, showIminuitMinosResult, showIminuitResultCorrmat, outputMinuitResult, EvaluateForMinuit, printLatexTable, printChi2nDoF, makeResidualPlot
from ResultSet import *
from iminuit import Minuit
import pylab as pl
from scipy import stats, special

outputpath = "outputs/"
        
def doAverage( reslist, pars, outputJSONname):
    """
    Main code to perform the combination
    reslist: the ResultList containg all ResultSets to be averaged
    pars: the names of the target parameters for the average
    """
    np.set_printoptions(5)
    
    #Create the EvaluateFotMinuit object which provides the FCN interface
    eval = EvaluateForMinuit( reslist, pars )

    # Get the dictionary with all parameters, their start values and their initial setepsize (errors)
    svals = reslist.getStartValues( pars )

    # Now get the numerical list of starting values out of the dictionary in the correct order for the Minuit constructor
    start =[ svals[p] for p in pars ]
            
    # Create Minimiser with special constructor.
    # The names and order of parameters is given by pars
    m = Minuit(eval.fcn, start, name=pars)
    m.errordef = 1.0
    m.print_level = 0
    m.tol = m.tol*0.0001
    
    #Fix or update any parameters you want to - examples below
    #m.values[pars.index('dms')] = 17.757
    #m.fixed[pars.index('dms')] = True
   
    #Do fit
    m.migrad()
    m.minos()
    
    #Save output as json
    outputMinuitResult( m, outputFileName=outputpath+outputJSONname)
    
    return m

#-------------- Main -------------
def main():
    #Set up input file list
    print('\nTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT')
    print('\ResultSet Combination Code ')
    
    #CP even measurement inputs - it can be one or more files
    inputfileGsDGsCPeven = ['../inputs/ResultList-tau-CPeven-Spring2020.json']
    
    #Define fit parameters
    parameters    = ["tausCPeven"]

    #Read result list and set averaging options
    reslist = readResultListJSON(inputfileGsDGsCPeven, inputpath="")
    reslist.setErrorTreatmentFlags( doStatCorr=False, doSyst=False, doSystCorr=False, doIntercorr=False )
    reslist.sanityCheck( )
          
    CPevenOutputFileName = "tausCPeven.json"
    minfit = doAverage( reslist, parameters, CPevenOutputFileName)
    
    #Print the minuit result for the averages
    showIminuitResult( minfit )
    
    #Minos errors
    showIminuitMinosResult( minfit )
    
    #Show the simple average for comparison
    reslist.showSimpleAverage(paramorder = parameters)
    
    #Show the final correlation matrix
    showIminuitResultCorrmat( minfit )
    
    #Print chi2/nDoF
    chisqndof = printChi2nDoF(reslist, minfit)

    #Make residuals plot all points
    makeResidualPlot( chisqndof, reslist, minfit, outputpath+'PullPlot.pdf' )
    
    
    return

main()
