\documentclass[10pt]{beamer}

\newcommand{\link}[2]{{\color{ferngreen}\href{#1}{#2}}}
\newcommand{\phis}{\ensuremath{\phi_{s}}}
\newcommand{\sign}{\text{sign}}
\def\CoCo{\texttt{CoCo}\xspace}
\def\invps{\ensuremath{{\mathrm{ \,ps^{-1}}}}\xspace}
\def\rad{\ensuremath{\mathrm{ \,rad}}\xspace}

%%%%%%%%%%%%%%%% Título/Autores %%%%%%%%%%%%%%%%%%%%%%%%
\title[]{\textcolor{ferngreen}{\bf Combination code (CoCo) tutorial}}
\author{{\fontsize{12pt}{20}\selectfont {\bf Veronika Chobanova}}}


%%%%%%%%%%%%%%%% Configurações %%%%%%%%%%%%%%%%%%%%%%%%
\input{configuracoes.tex}

%%%%%%%%%%%%%% Notas de rodapé %%%%%%%%%%%%%%%%%%%%

\renewcommand{\insertshorttitle}{{\fontsize{8pt}{8}\selectfont \textcolor{ferngreen}{Combination code (CoCo) tutorial}}}

\renewcommand{\insertshortauthor}{{\fontsize{8pt}{8}\selectfont {\bf Veronika Chobanova}}}
 \newcommand{\insertevent}{{\fontsize{8pt}{8}\selectfont{CoCo tutorial}}}
\date{ 26 October 2022}

\setbeamertemplate{footline}{
   \begin{beamercolorbox}[ht=3ex,leftskip=2mm,rightskip=2mm]{}
   % ht: altura
    \textcolor{ferngreen}{\hrule height 3pt}
    \vspace{0.1cm}
    \insertshortauthor \hfill \insertshorttitle  \hfill  \insertevent {\fontsize{8pt}{8}\selectfont {\hspace{0.4cm}\insertframenumber}}
   \end{beamercolorbox}
   \vspace*{0.1cm}
}

\begin{document}

\fontsize{10pt}{11pt}\selectfont

 \addtocounter{framenumber}{-1}
%------------------------------------------------------
  \begingroup
  \setbeamertemplate{footline}{}
 {\logotitle
 \begin{frame}
 \maketitle
 \end{frame}
 }
 \endgroup
\nologo
%------------------------------------------------------

\begin{frame}{\texttt{CoCo} overview}
  {\link{https://gitlab.cern.ch/vchobano/combination-code/}{\texttt{gitlab}} $\bullet$ 
   \link{https://gitlab.cern.ch/vchobano/combination-code/-/jobs/20951376/artifacts/file/documentation/main.pdf}{documentation}}
   
  {\it \texttt{CoCo} is a generic averaging code to combine an arbitrary set of (experimental) results under consideration of the statistical errors and their correlations, and systematic errors and correlations, both between the parameters of one measurement and different measurements.}
 \begin{itemize}
  \item[]
  \item Code is \texttt{python} based
  \item Requires \texttt{python3, pylab, matplotlib} and \texttt{iminuit2}
  \item Written by Peter Clarke and Veronika Chobanova, prompted by averages for LHC(b) measurements of $\phi_{s}$
  \item[] $\rightarrow$ drives to some extent examples and available features
  \item[]
 \end{itemize}
 
\end{frame}

 \begin{frame}{\texttt{CoCo} overview}
 \texttt{CoCo} provides averaging features such as
  \begin{itemize}
    \item multidimensional averages
    \item correlations between measurements and parameters
    \item correlations between single systematic uncertainties
    \item conversion between related parameters
    \item usage of output of one average as an input to another...
   \item[]
%    \item What \texttt{CoCo} cannot do (yet)
%    \begin{itemize}
%     \item Automated plots of inputs and averages
%     \item[] $\rightarrow$ Though overall comparison plot on all inputs and averages exists
%     \item Automatic printouts of bibliography 
%   \end{itemize}
  \end{itemize}
  
 HFLAV averages in \texttt{CoCo}
 \begin{itemize}
  \item $\Delta m_{s}$: verified against \texttt{COMBOS}
  \item $\tau(B^{0}_{s})$, $\Delta\Gamma_{s}$ and $\phi_{s}$: verified against previous code
 \end{itemize}
 \end{frame}

\begin{frame}{Goals of this tutorial}
 \begin{itemize}
  \item Explain the functioning principles of \texttt{CoCo}
  \item Provide a guide to the most important features
  \item Give a hands-on experience through example averages
 \end{itemize}
 \end{frame}
\begin{frame}{Getting started}
\begin{itemize}
 \item Make sure you have prerequisites:\\
 \texttt{python3, pylab, matplotlib} and \texttt{iminuit2}
 \item Clone repository
 \item[] \texttt{\$ git clone --recursive ssh://git@gitlab.cern.ch:7999/vchobano/combination-code.git}
 \item[] \texttt{\$ cd combination-code}
 \item Run example to verify it works
 \item[] \texttt{\$ cd code/python}
 \item[] \texttt{\$ python CombineResultsExample.py}
 \item[]
 \item If you see a folder \texttt{outputs} with \texttt{json}, \texttt{tex} etc files, you are ready to go
\end{itemize}
\end{frame}

\begin{frame}{How \texttt{CoCo} works}
\scriptsize{\texttt{CoCo} uses as inputs \texttt{ResultSet}s in \texttt{json} consisting of {\it central values, errors} and {\it correlation matrices}
 \includegraphics[trim=0cm 13cm 0cm 0cm, clip=true, height=0.8\textheight]{ResultSetExample.png}}
\end{frame}

\begin{frame}{How \texttt{CoCo} works}
 \begin{itemize}
  \item As a first step, define a desired set of output parameters, $\overrightarrow{P}^{\rm fit}$
  \item Any average boils down to minimizing a $\chi^2$  wrt $\overrightarrow{P}^{\rm fit}$ constructed as
  \begin{equation}
   \chi^2 = \Delta^{T} E^{-1} \Delta
  \end{equation}
  \begin{itemize}
  \item $E^{-1}$: overall covariance matrix
  \item $\Delta = \overrightarrow{V}^{\rm meas} - \overrightarrow{V}^{\rm fit}$: a vector of differences between input central values, $\overrightarrow{V}^{\rm meas}$, and fit parameters, $\overrightarrow{V}^{\rm fit}$
  \item $\overrightarrow{V}^{\rm meas}$ either matches parameters found in or can be transformed into parameters in $\overrightarrow{P}^{\rm fit}$
  \item $\overrightarrow{V}^{\rm fit}$ is a vector of fit parameters created from $\overrightarrow{P}^{\rm fit}$, with the same length as $\overrightarrow{V}^{\rm meas}$
  \end{itemize}
  \item Output same format as inputs, can be directly used in another average
 \end{itemize}
\end{frame}

\begin{frame}{How \texttt{CoCo} works}
\begin{itemize}
 \item For the case of measurements A, B, C..., with parameters $\overrightarrow{V}_A$, $\overrightarrow{V}_B$, $\overrightarrow{V}_C$... and overall covariance matrices (include statistical and systematic uncertainties) $E_A$,  $E_B$,  $E_C$... 
\end{itemize}

 $$
\overrightarrow{V}^{meas}=
\begin{bmatrix}
\label{uber-vector}
\overrightarrow{V}_A \\
\overrightarrow{V}_B \\
\overrightarrow{V}_C \\
... \\
\end{bmatrix}
\hspace{1cm}
E=
\begin{bmatrix}
\label{uber-matrix1}
E_A     &  &  & ... \\
 & E_B   & & ... \\
 & & E_C   & ... \\
...     & ...   & ...   & ... \\
\end{bmatrix}
$$
\begin{itemize}
 \item It is also possible to correlate systematic uncertainties between measurements by providing a recipy to construct covariance matrices between measurements, $C_{AB}$, $C_{AC}$, $C_{BC}$..., in which case
 $$E=
\begin{bmatrix}
\label{uber-matrix2}
E_A     & C_{AB}& C_{AC}& ... \\
C_{AB}  & E_B   & C_{BC}& ... \\
C_{AC}  & C_{BC}& E_C   & ... \\
...     & ...   & ...   & ... \\
\end{bmatrix}.
$$

\end{itemize}


\end{frame}

\begin{frame}{\texttt{CoCo} code structure}
 \begin{itemize}
  \item {\bf Base code} consists of three files with a total of $<1400$ lines of code organized in three scripts
  \begin{itemize}
   \item \texttt{ResultSet.py} is the core of the code where the two main classes are defined, \texttt{ResultSet} and \texttt{ResultSetList}
   \item \texttt{HelperFunctions.py} provide mainly functions for printouts of results to files and in the terminal
   \item \texttt{IntercorrelationMaps.py} handles correlations between measurements (to be discussed later in more details)
  \end{itemize}
  \item {\bf The user} needs to provide
  \begin{itemize}
   \item An {\bf averaging script} to define inputs, parameters, averaging function etc. Example provided: \texttt{code/python/CombineResultsExample.py}\\
   Further examples in this tutorial, see \texttt{tutorial/Example{1,2,3}}
   \item A {\bf set of results to average} organized as \texttt{ResultSet}s in \texttt{json} files, see for example \texttt{code/inputs/     ResultList-Example-JpsiKK.json}
   \item (Optional) An {\bf intercorrelation map} in case of correlating measurements
   \item (Optional) A {\bf set of translators} in case of averaging related parameters
  \end{itemize}


 \end{itemize}

\end{frame}


\begin{frame}{Example 1: Average of \it{CP}-even $\tau(B_{s}^{0})$}
 \begin{itemize}
  \item Simplest case of a 1D average from two measurements without correlations
  \item To run
  \item[] \texttt{\$ cd combination-code/tutorial/Example1}
  \item[] \texttt{\$ python Example1Average.py}
  \item Note both inputs in the same file organized in a \texttt{ResultList}, \texttt{../inputs/ResultList-tau-CPeven-Spring2020.json}
  \item[]
 \end{itemize}
 \begin{minipage}{0.5\textwidth}
 Simple or naive average same as \texttt{Minuit} result as no correlations considered\\
 
   \includegraphics[width=\textwidth]{Example1Output.png}\\
 \end{minipage}
 \begin{minipage}{0.02\textwidth}
  \mbox{}
 \end{minipage}
 \begin{minipage}{0.45\textwidth}
 Pull plot to visualise inputs compared to average\\
   \includegraphics[width=\textwidth]{../Example1/outputs/PullPlot.pdf}
 \end{minipage}

\end{frame}
\begin{frame}{Example 1: Average of \it{CP}-even $\tau(B_{s}^{0})$}
\begin{itemize}
 \item Result output saved under \texttt{outputs/tauCPeven.json}
 \item Same format as inputs, note \texttt{StatisticalCorrelationMatrix} in this case means the full correlation matrix that comes out of the fit
 \item[]
\end{itemize}
\centering
   \includegraphics[width=0.7\textwidth]{Example1Json.png}

\end{frame}


\begin{frame}{Correlating measurements}
 \begin{itemize}
  \item To correlate measurements, the user needs to provide a \texttt{json} file with details
  \end{itemize}
  \centering
  \includegraphics[width=0.9\textwidth]{InterCorrelations.png}
  \begin{itemize}
   \item \texttt{``ResultSetLabels``}: Specifies which results to intercorrelate.\\
   Need two entries at least, if \texttt{"All"} specified, will correlate all measurements
  \item[]
   \item \texttt{''ParameterEquivalenceLists``}: To specify parameters that are named differently but are equivalent for averaging purposes
   \end{itemize}


\end{frame}

\begin{frame}{Correlating measurements}
\begin{itemize}
   \item \texttt{''IntercorrelationMaps``}: Dictionary with names of systematics to correlate and related attributes
   \begin{itemize}
    \item \texttt{scale}: To scale correlations, mainly for testing. Otherwise set to 1
    \item \texttt{ictype}: Possible options include \texttt{DIAG} and \texttt{FULL}, relevant for multidimensional averages.
    \\ \texttt{DIAG}: Correlate only same/equivalent parameters between measurements, i.e. covariance matrices between measurements, e.g. $C_{AB}$, $C_{BC}$ would be diagonal
    \\ \texttt{FULL}: Correlate all parameters between measurements, including different parameters, i.e. covariance matrices between measurements could contain non-zero off-diagonal elements
    \item[]

   \end{itemize}
    $$E=
\begin{bmatrix}
\label{uber-matrix2}
E_A     & C_{AB}& C_{AC}& ... \\
C_{AB}  & E_B   & C_{BC}& ... \\
C_{AC}  & C_{BC}& E_C   & ... \\
...     & ...   & ...   & ... \\
\end{bmatrix}.
$$
\end{itemize}

\end{frame}


\begin{frame}{Correlating measurements}
\begin{itemize}
   \item \texttt{''IntercorrelationMaps``}: Dictionary with names of systematics to correlate and related attributes
   \begin{itemize}
    \item \texttt{strategy}: Strategy to calculate correlation between two parameters (1 and 2) from two \texttt{ResultSet}s (A and B) in the \texttt{FULL} case. Possible options include \\
    $\texttt{MIN}=\text{min}(|\rho^{A}_{12}|,|\rho^{B}_{12}|)\cdot(\sign(\text{min}(|\rho^{A}_{12}|,|\rho^{B}_{12}|)))$\\
    $\texttt{MAX}=\text{max}(|\rho^{A}_{12}|,|\rho^{B}_{12}|)\cdot(\sign(\text{max}(|\rho^{A}_{12}|,|\rho^{B}_{12}|))$\\
    $\texttt{AVG}=(\rho^{A}_{12}+\rho^{B}_{12})/2$\\
    $
      \texttt{SQRT}= 
  \begin{cases}
      \sign(\rho^{A}_{12})\cdot\sqrt{\rho^{A}_{12}\cdot\rho^{B}_{12}},& \text{if } \sign(\rho^{A}_{12})=\sign(\rho^{B}_{12})\\
      \texttt{AVG},              & \text{otherwise}
  \end{cases}
  $
   \end{itemize}
\end{itemize}

\end{frame}

\begin{frame}{Example 2: Average of $\Delta m_{s}$}
\begin{minipage}{0.65\textwidth}
\begin{itemize}
 \item A simple 1D average with corrrelated systematic uncertainties
 \item Correlating \texttt{''pzscales``} in all LHCb results
 \item[]
\end{itemize}
  \includegraphics[width=\textwidth]{InterCorrelations.png}
\put(-230,70){\scriptsize \textcolor{red}{\underline{Intercorrelations \texttt{json} file}}}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\includegraphics[width=\textwidth]{Example2Json.png}
\put(-100,140){\scriptsize \textcolor{red}{\underline{From input \texttt{json} file}}}
\end{minipage}
\includegraphics[width=\textwidth]{Example2AddingCorrelations.png}
\put(-350,70){\scriptsize \textcolor{red}{\underline{Set up in averaging script}}}
\end{frame}

\begin{frame}{Example 2: Average of $\Delta m_{s}$}
\begin{itemize}
 \item Informative printout on correlations and measurements considered
\end{itemize}
 \centering
\includegraphics[width=\textwidth]{Example2InterCorrOutput.png}
\begin{itemize}
 \item  Note simple average clearly different from fit result with correlations
\end{itemize}
 \centering
\includegraphics[width=0.6\textwidth]{Example2Output.png}
 
\end{frame}

\begin{frame}{Averaging related parameters}
 \begin{itemize}
  \item \texttt{CoCo} provides a functionality to average related parameters 
  \item E.g. if experiment A provides a measurements of a lifetime parameter $\Gamma^{A}$ and experiment B provides $\tau^{B}$, one can average them to a common $\Gamma$ or $\tau$
  \item To do this, you need to provide a \texttt{Translator}
  \item Translators are functions that transform the parameters into one another allowing to combine them into one parameter
  \item The transformation considers automatically the change in the uncertainties and correlations of the parameter wrt the rest of the parameters
  \item[] $\rightarrow$ no need to manually convert parameters into one another beforehand
 \end{itemize}

\end{frame}


\begin{frame}{Example 3: Usage of translators and multidimensional averages}
\begin{itemize}
 \item Example 3 is a simplified average of the $B_{s}^{0}\to J/\psi KK$ measurements by the LHC
 \item The experiments quote different sets of parameters which can still be combined
 \item E.g. CMS and LHCb quote the polarisation fractions $|A_{\perp}|^2$ and $|A_{0}|^2$ and ATLAS quotes $|A_{\parallel}|^2$ and $|A_{0}|^2$
 \item[] $\rightarrow$ fit $|A_{\perp}|^2$ in the combination transforming $|A_{\parallel}|^2 = 1-|A_{0}|^2-|A_{\perp}|^2$
 \item[]
\end{itemize}
\centering
\includegraphics[width=0.25\textwidth]{ATLASAparaSq.png}
\includegraphics[width=0.25\textwidth]{CMSAperpSq.png}
\includegraphics[width=0.25\textwidth]{LHCbAperpSq.png}
\put(-250,40){\small \textcolor{red}{ATLAS}}
\put(-160,40){\small \textcolor{red}{CMS}}
\put(-70,40){\small \textcolor{red}{LHCb}}
\end{frame}


\begin{frame}{Example 3: Usage of translators and multidimensional averages}
Including translators
 \begin{itemize}
  \item Add to module \texttt{Translators.py}
  \item Call them in the averaging function
  \item[] $\rightarrow$ need to provide a dictionary with fit parameter names and values
  
 \end{itemize}
{\footnotesize \textcolor{red}{\underline{Translator for $|A_{\parallel}|^2$ in \texttt{Translators.py}}}}\\
\includegraphics[width=0.8\textwidth]{AparaTranslator.png}

{\footnotesize \textcolor{red}{\underline{Adding translators to averaging script}}}\\
\includegraphics[width=0.7\textwidth]{AddingTranslators.png}

{\footnotesize \textcolor{red}{\underline{Fit parameters - note no $|A_{\parallel}|^2$}}}\\
\includegraphics[width=1.\textwidth]{FitParsPhis.png}
\end{frame}

\begin{frame}{Example 3: Usage of translators and multidimensional averages}
 \begin{itemize}
  \item Another use case of translators is to produce averages of related parameters
  \item E.g. easily do  ($\tau_{s}$, $\Delta\Gamma_{s}/\Gamma_{s}$) and ($\tau_{sH}$,$\tau_{sL}$) from ($\Gamma_{s}$, $\Delta\Gamma_{s}$)
  \item[] 
 \end{itemize}
 \centering
\includegraphics[width=\textwidth]{LifetimeTranslators.png}
\end{frame}



\begin{frame}{Final remarks}
Other features
\begin{itemize}
 \item External constraints: Just add a \texttt{ResultSet} with the central value and error un the parameter(s) you want to constrain
 \item \texttt{printLatexTable}: Prints final correlation table in a \texttt{LaTeX} file
 \item[]
\end{itemize}

\texttt{CoCo} maintenance
 \begin{itemize}
%   \item \texttt{CoCo} provides basic functionalities for averaging code
  \item Extensions are possible and straight forward to implement as code based on \texttt{python}
  \item Feedback and suggestions always welcome 
  %\item Currently two people develop and maintain the code, therefore limited availability to introduce new functionalities
  \item New contributions also welcome
  \item[]
 \end{itemize}
\centering
\Large{Thank you!}
\end{frame}



\end{document}
\end{document}
