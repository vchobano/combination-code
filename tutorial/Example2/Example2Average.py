###############################
### 2020 Veronika Chobanova ### 
###############################

import sys
import json
sys.path.append("../../code/python/")
from HelperFunctions import showIminuitResult, showIminuitMinosResult, showIminuitResultCorrmat, outputMinuitResult, EvaluateForMinuit, printLatexTable, printChi2nDoF, makeResidualPlot
from ResultSet import *
from iminuit import Minuit
import pylab as pl

outputpath = "./outputs"
        
def doAverage( reslist, pars, outputJSONname):
    np.set_printoptions(5)
   
    #Create the FCN.
    eval = EvaluateForMinuit( reslist, pars )

    # Get the dictionary with all parameters, their start values and their initial setepsize (errors)
    svals = reslist.getStartValues( pars )

    # Now get the numerical list of starting values out of the dictionary in the correct order for the Minuit constructor
    start =[ svals[p] for p in pars ]
            
    # Create Minimiser with special constructor.
    # The names and order of parameters is given by pars
    m = Minuit(eval.fcn, start, name=pars)
    m.errordef = 1.0
    m.print_level = 0
    m.tol = m.tol*0.0001
    
    #Do fit
    print('\n Starting Minuit fit')
    m.migrad()
    print(m.values)
    print(m.errors)
 
    #Print the final averages
    showIminuitResult( m )

    m.minos( )
    showIminuitMinosResult( m )
    
    #Show the simple average for comparison
    reslist.showSimpleAverage(paramorder = pars)
 
    #Show the final correlation matrix
    showIminuitResultCorrmat( m )
    
    #Print chi2/nDoF
    chisqndof = printChi2nDoF(reslist, m)

    #output the full result in JSON format for use in a subsequent average
    reslist.outputSimpleAverage(outputFileName='LHCbSimpleAverageResultSet.json', title='Simp.Avg.' )

    outputMinuitResult( m, outputFileName="outputs/"+outputJSONname, title=outputJSONname.replace(".json", ""), minos=True )
    
    res = readResultListJSON([outputJSONname], inputpath="outputs/")
    
    latexLabels = {"dms": r"\Delta m_s"}
    printLatexTable(m, latexLabels, outputFileName='outputs/DmsCombinationCorrmatTable'+outputJSONname.replace(".json", ".tex"))
    
    #Make residuals plot all points
    makeResidualPlot( chisqndof, reslist, m, outputpath+'PullPlot.pdf' )
    
    return m, res

#-------------- Main -------------
def main():
    #Set up input file list
    print('\nTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT')
    print('\ResultSet Combination Code ')
    
    #Inputs
    inputfile = ["../inputs/Dms.json"]
    parameters = ["dms"]
    
    #This is where you configure your list of input JSON files to configure any intercorrelations  (optional)
    intercorrelationFileList = [
        'IntercorrelationMaps-Dms.json'
    ]
    
    #Show whats in the ResultList of ResuiltSets
    reslist = readResultListJSON(inputfile, inputpath='', icfilelist=intercorrelationFileList)

    reslist.setErrorTreatmentFlags( doStatCorr=True, doSyst=True, doSystCorr=True, doIntercorr=True )
    reslist.showAll()  #Show all systematic errors and full correlation matrix
    reslist.show()     #Just show parameters and overall correlation and covariance matrices
    reslist.sanityCheck( )
    
    print('\nMain program has set up this Input file(s): ',inputfile)
    
    print('\nThe combination has been configured with these intercorrelations file(s): ',intercorrelationFileList)
    
    #Show the set of unique parameters for info
    #uniquepars = reslist.getUniqueParameters()
    #print('\n It has unique parameters: ', ['{:10s}'.format(par) for par in uniquepars])
           
    # Make the combination
    m, res = doAverage( reslist, parameters, "Example2.json" )
          
    res.label = "Combined"
          
    #Print log file
    inputString = "Inputs Dms Average" + "\n"
    inputString += "------------------" + "\n"
    
    for r in reslist.resultList:
        inputString += str(" ".join(r.description)) + "\n"
        id_DMs = r.getParameterIndex("dms")
        inputString += str(r.getValues()[id_DMs]) + " +/- " + str(round(r.paramErrors[id_DMs],3)) + " (stat) +/- " + str(round(r.getSystErrors()[id_DMs],3)) + " (syst) "+ "\n"
    
    rAvAll = res.resultList[0]
    id_DMs = rAvAll.getParameterIndex("dms")
    covMatTot = rAvAll.getCovmat()
    
    inputString += "------------------" + "\n"
    inputString += "Dms Average" + "\n"
    inputString += "------------------" + "\n"
    
    reslist.setErrorTreatmentFlags( doStatCorr=True, doSyst=False, doSystCorr=False, doIntercorr=False )
    m, res = doAverage( reslist, parameters, "tmp.json" )
   
    rAv = res.resultList[0]

    id_DMs = rAv.getParameterIndex("dms")
    covMatStat = rAv.getCovmat()
    
    inputString += "dms = " + str(round(rAvAll.getValues()[id_DMs],3)) + " +/- " 
    
    inputString += str(round(np.sqrt(covMatStat[id_DMs,id_DMs]),3))  + " (stat) +/- " 
    
    inputString += str(round(np.sqrt(covMatTot[id_DMs,id_DMs]-covMatStat[id_DMs,id_DMs]),3)) + " (syst)"
    
    print(inputString)
    print(inputString, file=open("DMS_Average.txt", "w"))
    
main( )
