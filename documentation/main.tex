\documentclass[a4paper,11pt]{article} 
\usepackage[utf8]{inputenc}
\usepackage{microtype}
\usepackage{lineno}  % for line numbering during review
\usepackage{xspace} % To avoid problems with missing or double spaces after
                    % predefined symbold
\usepackage{caption} %these three command get the figure and table captions automatically small
\renewcommand{\captionfont}{\small}
\renewcommand{\captionlabelfont}{\small}
\usepackage[left=2cm,top=2cm,bottom=2cm,right=2cm]{geometry}

%% Graphics
\usepackage{graphicx}  % to include figures (can also use other packages)
\usepackage{color}
\usepackage{colortbl}
\graphicspath{{./figs/}} % Make Latex search fig subdir for figures
% \DeclareGraphicsExtensions{.pdf,.PDF,.png,.PNG}   % not needed

%% Math
\usepackage{amsmath} % Adds a large collection of math symbols
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{upgreek} % Adds in support for greek letters in roman typeset

\usepackage{booktabs}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{rotating}
\usepackage{listings}
\usepackage{lipsum}
\usepackage[htt]{hyphenat}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\newcommand{\CP}{{\it CP }}
\newcommand{\rset}{{\it ResultSet }}
\newcommand{\rsets}{{\it ResultSets }}
\newcommand{\Vv}{\overline{V}}             
\newcommand{\Par}{\overline{P}}     
\newcommand{\tev}{\ensuremath{\mathrm{\,Te\kern -0.1em V}}\xspace}
\newcommand{\decay}[2]{\ensuremath{#1\!\to #2}\xspace}           
\def\phis      {\ensuremath{\phi_{s}}\xspace}          
\def\PB      {\ensuremath{B}\xspace}     
\def\B       {{\ensuremath{\PB}}\xspace}
\def\proton      {{\ensuremath{p}}\xspace}
\def\Pd      {\ensuremath{\mathrm{d}}\xspace}  
\def\Ps      {\ensuremath{\mathrm{s}}\xspace}  
\def\dquark    {{\ensuremath{\Pd}}\xspace}
\def\squark    {{\ensuremath{\Ps}}\xspace}
\def\Bd {{\ensuremath{\B^0_\dquark}}\xspace}
\def\Bs {{\ensuremath{\B^0_\squark}}\xspace}
\def\Bsb {{\ensuremath{\overline{B}^0_\squark}}\xspace}
\def\PK      {\ensuremath{\mathrm{K}}\xspace}  
\def\kaon    {{\ensuremath{\PK}}\xspace}
\def\pip      {{\ensuremath{\pi^+}}\xspace}
\def\pim      {{\ensuremath{\pi^-}}\xspace}
\def\Kp      {{\ensuremath{\kaon^+}}\xspace}
\def\Km      {{\ensuremath{\kaon^-}}\xspace} 
\def\PJ      {\ensuremath{\mathrm{J}}\xspace}                
\def\Ppsi        {\ensuremath{\psi}\xspace}                   
\def\jpsi     {{\ensuremath{{\PJ\mskip -3mu/\mskip -2mu\Ppsi}}}\xspace}
\def\lhcb   {\mbox{LHCb}\xspace}

\title{Combination code (CoCo) for averaging a set of measurements}

\date{\today}

\begin{document}

\maketitle
\centering
Peter Clarke$^1$, Veronika Chobanova$^2$\\
\flushleft
{\it $^1$University of Edinburgh, Edinburgh, United Kingdom\\
$^2$Instituto Galego de F\'isica de Altas Enerx\'ias (IGFAE), Universidade de Santiago de Compostela, Santiago de Compostela, Spain \\
}

\section{Introduction}
This is a generic averaging code to combine an arbitrary collection of (experimental) measurements. It allows the inclusion of the statistical errors and their correlations, as well as systematic errors and their correlations. Correlations are handled both between the parameters of one set of measurements and between different sets of  measurements. A brief explanation of the averaging method is given in Sect.~\ref{sec:AveragingMethod} followed by a guide of an example code in Sect.~\ref{sec:ExampleCode}.

\section{Averaging method}
\label{sec:AveragingMethod}
In this section, details of the general averaging method for any set of measurements is presented. 

\subsection{{\it ResultSet}s}
\label{subsec:ResultSets}

The output of an analysis (or experiment), or an already combined set of analyses from an experiment, is referred to as a {\it ResultSet}. 
A {\it ResultSet} is stored in a \texttt{json} file, and contains a set of parameter values, their statistical-errors and correlation matrix, 
plus any systematic errors and their corresponding correlation matrices, if they exist. 
Example \texttt{json} files are provided in \texttt{code/inputs/}. 

The parameters are provided as a list of dictionaries with a name (\texttt{"Name"}), a central value (\texttt{"Value"}) and an error (\texttt{"Error"}, typically statistical). A statistical correlation matrix between the parameters is provided through a nested list as \texttt{"StatisticalCorrelationMatrix"}. Note that the order of the individual correlations must follow the order in the \texttt{"Parameter"} list. Next, systematic uncertainties and possible correlations can be provided as a list \texttt{"SystematicErrors"}. Again, the order of uncertainties and correlations must follow that of the \texttt{"Parameter"} list.

An arbitrary set of these  {\it ResultSet}s, organised in a {\it ResultList}, are used as the input to the averaging program. Each can containin any number of parameters. The {\it ResultSet}s are not required to contain the same parameters. It is also possible to average parameters from different {\it ResultSets} which can be transformed into each other (see Sect.~\ref{sec:Translators}).

\subsection{General averaging procedure}
\label{subsec:CombGeneral}



First, a vector of measurement values, $\overline{V}_{all}^{meas}$,  is created from the set of {\it ResultSet}s A, B, C... \\
$$
\Vv_{all}^{meas}=
\begin{bmatrix}
\label{uber-vector}
\Vv_A \\
\Vv_B \\
\Vv_C \\
... \\
\end{bmatrix}.
$$
where $\Vv_A$ are the measurement values of all parameters in {\it ResultSet} A ...etc.

Next an overall covariance matrix, $E_{all}$,  which is block-diagonal in the {\it ResultSet}s, is created from the errors and correlation matrices in each {\it ResultSet}. 
$$E_{all}=
\begin{bmatrix}
\label{uber-matrix1}
E_A     &  &  & ... \\
 & E_B   & & ... \\
 & & E_C   & ... \\
...     & ...   & ...   & ... \\
\end{bmatrix},
$$
where $E_A$ is the square matrix with dimension equal to the number of parasmeters measured in {\it ResultSet} A.
This includes the statistical errors on the parameters with a correlation matrix provided in the \texttt{json} file as \texttt{"Error"} in the \texttt{"Parameter"} list and as \texttt{"StatisticalCorrelationMatrix"}, respectively. Correlations between systematic uncertainties are taken into account as \texttt{"SystematicCorrelationMatrix"} for each entry in the \texttt{ "SystematicErrors"} list. If no correlation matrix is provided, the systematic errors are considered uncorrelated and the resulting covariance matrix is diagonal.

Next, it is possible to correlate uncertainties between {\it ResultSet}s, referred to as {\it inter-correlations}. These are added as off-block-diagonal sub-matrices $C_{IJ}$ ($I\neq J$). 
$$E_{all}=
\begin{bmatrix}
\label{uber-matrix2}
E_A     & C_{AB}& C_{AC}& ... \\
C_{AB}  & E_B   & C_{BC}& ... \\
C_{AC}  & C_{BC}& E_C   & ... \\
...     & ...   & ...   & ... \\
\end{bmatrix}.
$$
The way of defining how the  $C_{AB}$ are formed based upon a supplementary set of \texttt{json} files is described in Section~\ref{subsec:CombSyst}

% Next, a vector, $\Vv_{all}^{fit}$, containing the combination output parameters, $\Par^{fit}$, is defined, in which each parameter either matches parameters found in $\Vv_{all}^{meas}$ or can be linearly transformed into one of them. 
Next, a desired set of combined output parameters, $\Par^{fit}$, is defined, where $\Par^{fit}$ either matches parameters found in $\Vv^{meas}_{all}$ or can be transformed into parameters in $\Vv_{all}$ (see Sect.~\ref{sec:Translators}). From $\Par^{fit}$, a vector of fit parameters, $\Vv_{all}^{fit}$, is created corresponding to each of the measured parameters, $\Vv_{all}^{meas}$. 

Finally, a difference vector is created,
 \begin{equation}
\overline{\Delta} = \Vv_{all}^{meas} - \Vv_{all}^{fit},
\end{equation}
and a $\chi^2$ is constructed according to
\begin{equation}
\chi^2 = \overline{\Delta}^T E_{all}^{-1}  \overline{\Delta}.
\end{equation}
The resulting $\chi^2$ is minimised with respect to $\Par^{fit}$ using \texttt{Minuit}~\cite{JAMES1975343}, determining a set of parameters, uncertainties and an overall correlation matrix. These can be used as input for future averages. 

\subsection{Correlations between systematic uncertainties in different \textbf{\it ResultSet}s}
\label{subsec:CombSyst}
 \noindent Inter-correlations between each common systematic uncertainty in two or more  {\it ResultSet}s are much harder to take account of in a fully generic way. For example one could specify each and every correlation in some \texttt{json} file with thousands of entries. This was not considerd to be useful.
 
 We have chosen instead to implement a set of pre-defined maps. Currently two types of maps are implemented: 
 \begin{enumerate}
\item {\it "Diagonal-only": } if parameter $X$ appears in {\it ResultSet}s A and B, the systematic errors in two sets are assumed to be s\% correlated in $C_{AB}$ between $X_A$ and $X_B$ but otherwise no correlation between different parameters  $X_A$ and $Y_B$, ($X\neq Y$).  The default is $s=100\%$. We have also allowed for the case that $X_A$ and $X_B$ may be effectively the same parameter for this purpose, but not actually have exactly the same names - we include a parameter equivalence feature to handle this specified in the \texttt{json} files.
\item {\it "Full":} for each pair of parameters  $X_A$ and $Y_B$ in {\it ResultSet}s A and B respectively, a search is performed for 
correlations $\rho_{X Y}$ declared within both A and B.  If correlations are found in both A and B, the smaller correlation is taken as the correlation between $X_A$ and $Y_B$ in $C_{AB}$.  If no such correlation is found in either A or B then no correlation is assumed. Again an arbitrary scale factor can be applied that is 100\% by default. Note that this automatically includes the {\it "Diagonal-only"} case above. The parameter equivalence feature is also included.
 \end{enumerate}
The type of inter-correlation for each systematic uncertainty must be provided in a \texttt{json} file(s), see an example in \texttt{code/inputs/InterCorrelationMaps-All.json}. The name of the inter-correlated systematic uncertainties must be the same in all {\it ResultSet}s and it must be specified whether the inter-correlation is {\it "Diagonal-only"} (\texttt{"DIAG"}) or {\it "Full"} (\texttt{"FULL"}). 

The \texttt{json} file formally applies to inter-correlations between  single pair of {\it ResultSet}s, whose labels appear in the files. However a "special" label=\texttt{"All"} is recognised, which causes the file to be applied across all {\it ResultSet}s. 
Thus, an arbitrary number of these files can be provided to specify limited inter-correlations between subsets of {\it ResultSet}s if required. However for simple use only a single file specifying label=\texttt{"All"} is needed.

The file also contains lists of parameters which might have different names in different {\it ResultSet}s, but which are considered as equivalent (i.e. effectively the same parameter) for the purposes of inter-correlation.  An example might be $\Gamma$ in one set and $\Gamma_s$ in another. This is implemented via a set of  {\it "ParameterEquivalenceLists"} included in the \texttt{json} files . An example is discussed in Sect.~\ref{sec:ExampleCode}.

\subsection{Controlling treatment of errors}
\label{sec:Errorflasgs}

It was found useful to be able to dynamically control how errors are treated in the average (for testing purposes). The program allows the user to set flags to easily switch on and off: (i) inclusion of Statistical Correlations, (ii) inclusion of Systematic Errors or not, (iii) inclusion of Systematic Correlation Matrices or not and (iv) whether inter-correlations are included or not.

\subsection{Translators}
\label{sec:Translators}
In some cases, different measurements of the same property would use different parametrisations of the parameters. Such is for instance a measurement of a particle lifetime, $\tau$, which can also be measured as a decay width, $\Gamma$, instead. The two parameters are related via $\Gamma = \hbar/\tau$. In such cases, the parameters can be averaged using {\it Translators}, which are functions acting on the parameters provided by the user as a separate module (see \texttt{code/python/TranslatorsExample.py}). These functions transform the parameters into one another allowing to combine them into one parameter. The transformation considers automatically the change in the uncertainties and correlations of the parameter with respect to the rest of the parameters.

It is obviously not possible to provide a pre-coded set of all possible translators. The user may therefore expect to provide additions to this module as required.


\section{Example code}
\label{sec:ExampleCode}
We provide an example of a combination of four independent measurements ({\it ResultSet}s) in \texttt{code/python} which is executed as
\begin{itemize}
\item[] \texttt{\$ python CombineResultsExample.py}
\end{itemize}
The example combines four measurements stored in two \texttt{json} files in \texttt{code/inputs/}, \texttt{ResultList-Example-JpsiKK.json} and \texttt{ResultList-Example-JpsiPiPi.json}
These are based on results published in Refs.~\cite{LHCb-PAPER-2014-019,LHCb-PAPER-2014-059,LHCb-PAPER-2019-013,LHCb-PAPER-2019-003} with example correlation matrices provided for illustration. 

A description of the functions within a module (e.g. \texttt{HelperFunctions}, \texttt{IntercorrelationMaps}, \texttt{TranslatorsExample}) can be obtained by running
\begin{itemize}
\item[]
\texttt{\$ pydoc <ModuleName>}
\end{itemize}

In this example, the four measurements contain the parameters:
\begin{itemize}
    \item {\it ResultSet} \texttt{"2012-JPsiKK"}: \texttt{"gamma", "deltaGammas", "AperpSq", "AzeroSq", "para", "perp", "phis", "lamb", "dms"}
    \item {\it ResultSet} \texttt{"2016-JPsiKK"}: \texttt{"gsgd", "deltaGammas", "AperpSq", "AzeroSq", "para", "perp",  "phis",  "lamb", "dms", "FS1", "FS2", "FS3", "FS4", "FS5", "FS6", "deltaS1", "deltaS2", "deltaS3", "deltaS4", "deltaS5","deltaS6"}
    \item {\it ResultSet} \texttt{"2012-JPsiPiPi"}:  \texttt{"lambJpsiPiPi", "phis"}
    \item {\it ResultSet} \texttt{"2016-JPsiPiPi"}: \texttt{"ghgd", "lambJpsiPiPi", "phis"}.
\end{itemize}
Note that the only common parameter between all sets is \texttt{"phis"}. In addition, the parameters \texttt{"gamma", "gsgd"} and \texttt{"ghgd"} are equivalent as reflected in \texttt{InterCorrelationMaps-All.json}. These correspond to \Bs and \Bd meson decay widths and differences, $\Gamma_{s}$, $\Gamma_{s}-\Gamma_{d}$ and $\Gamma_{\rm H}-\Gamma_{d}$, respectively. Equivalent parameters are considered as the same parameter when inter-correlating {\it ResultSet}s. They are converted into one another through the translators defined in \texttt{TranslatorsExample.py}.
Furthermore, the \Bd meson decay width~\cite{HFLAV18} is an external Gaussian constraint to these measurements, which is added through a {\it ResultSet} in a separate \texttt{json} file, \texttt{ResultList-Gammad.json}. 

The set of independent parameters to be averaged is defined in \texttt{CombineResultsExample.py} as\\
\texttt{pars = ['phis', 'lamb','lambJpsiPiPi','gamma', 'deltaGammas', 'dms', 'AperpSq', 'AzeroSq', 'perp', 'para', 'FS1', 'FS2', 'FS3', 'FS4', 'FS5', 'FS6', 'deltaS1', 'deltaS2', 'deltaS3', 'deltaS4', 'deltaS5', 'deltaS6', 'gammaD']}.\\
Note that "\texttt{gamma", "gsgd"} and \texttt{"ghgd"} are all averaged into one parameter, \texttt{"gamma"}.

The user can choose whether to include statistical, systematic or inter-correlations in the average by setting  
\texttt{doStatisticalCorrelations, doSystematics, doSystematicCorrelations, doIntercorrelations} to \texttt{True} or \texttt{False}.

The averaged result is saved as a {\it ResultSet}, which can be used in a subsequent average. A pull plot of the input parameters with respect to the result as well as the fit $\chi^2$ per degrees of freedom is provided for information.

The example code has been written so that a new user only needs to change file names and flags in one function (\texttt{main}) to get going. All output is controlled from here as well. Optional access to another function is provided (\texttt{doAverage}) to allow the user to change Minuit behaviour should they wish, but it is not necessary to modify this to get going.

\clearpage
\bibliographystyle{unsrt}
\bibliography{main.bib}
\end{document}
 