# Combination Code
This is a generic averaging code to combine an arbitrary set of (experimental) results under consideration of the statistical errors and their correlations, and systematic errors and correlations, both between the parameters of one measurement and different measurements. The code documentation providing detailed instructions to the example code is available <a href="https://gitlab.cern.ch/vchobano/combination-code/builds/artifacts/master/file/documentation/main.pdf?job=compile_documentation" target="_blank">here</a>. 

A tutorial is available <a href="https://gitlab.cern.ch/vchobano/combination-code/builds/artifacts/master/file/tutorial/slides/CoCoTutorial.pdf?job=compile_tutorial" target="_blank">here</a>

## Set up
To get started, clone the repository.
```
$ git clone --recursive ssh://git@gitlab.cern.ch:7999/vchobano/combination-code.git
$ cd combination-code
```

The code requires **Python 3**, **pylab**, **scipy**, **matplotlib** and **iminuit 2**

## Documentation
There is a .tex documents supplied in `documentation/` that explains
how the code works and gives more information on how ot use it

## Example combination
An example code is provided in `code/`. To run it,
```
$ cd code/python
$ python CombineResultsExample.py
```


